package com.demo.jianjunhuang.recyclerviewdemo;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.demo.jianjunhuang.recyclerviewdemo.MAdapter.ViewHolder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity
    implements MAdapter.OnItemClickListener, OnClickListener {

  private RecyclerView recyclerView;

  private List<User> users = new ArrayList<>();

  private Button addBtn;
  private Button delBtn;

  private MAdapter mAdapter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    initView();
    initData();
    initListener();
    initRecyclerView();
  }

  private void initData() {
    for (int i = 0; i < 20; i++) {
      users.add(new User("", i + ""));
    }
  }

  private void initListener() {
    addBtn.setOnClickListener(this);
    delBtn.setOnClickListener(this);
  }

  private void initRecyclerView() {
    //设置方向
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    //StaggeredGridLayoutManager layoutManager =
    //    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
    recyclerView.setLayoutManager(layoutManager);

    //设置边界线
    DividerItemDecoration dividerItemDecoration =
        new DividerItemDecoration(this, LinearLayout.VERTICAL);
    dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.item_bottom));
    recyclerView.addItemDecoration(dividerItemDecoration);
    //设置动画
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    mAdapter = new MAdapter(users);
    mAdapter.setOnItemLongClickListener(this);
    recyclerView.setAdapter(mAdapter);
  }

  private void initView() {
    recyclerView = (RecyclerView) findViewById(R.id.rv_show);
    addBtn = (Button) findViewById(R.id.btn_add);
    delBtn = (Button) findViewById(R.id.btn_del);
  }

  @Override public void click(View view, ViewHolder holder) {
    User user = users.get(holder.getAdapterPosition());
    Toast.makeText(this, user.getName(), Toast.LENGTH_SHORT).show();
  }

  @Override public void onClick(View v) {
    switch (v.getId()) {
      case R.id.btn_add: {
        users.add(new User("", "add " + new Date().toString()));
        mAdapter.notifyDataSetChanged();
        break;
      }
      case R.id.btn_del: {
        users.remove(0);
        mAdapter.notifyDataSetChanged();
        break;
      }
    }
  }
}
