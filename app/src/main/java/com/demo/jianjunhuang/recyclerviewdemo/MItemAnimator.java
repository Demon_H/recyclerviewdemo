package com.demo.jianjunhuang.recyclerviewdemo;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * @author jianjunhuang.me@foxmail.com
 * @since 2017/6/3.
 */

public class MItemAnimator extends RecyclerView.ItemAnimator {
  @Override public boolean animateDisappearance(@NonNull ViewHolder viewHolder,
      @NonNull ItemHolderInfo preLayoutInfo, @Nullable ItemHolderInfo postLayoutInfo) {
    return false;
  }

  @Override public boolean animateAppearance(@NonNull ViewHolder viewHolder,
      @Nullable ItemHolderInfo preLayoutInfo, @NonNull ItemHolderInfo postLayoutInfo) {
    return false;
  }

  @Override public boolean animatePersistence(@NonNull ViewHolder viewHolder,
      @NonNull ItemHolderInfo preLayoutInfo, @NonNull ItemHolderInfo postLayoutInfo) {
    return false;
  }

  @Override
  public boolean animateChange(@NonNull ViewHolder oldHolder, @NonNull ViewHolder newHolder,
      @NonNull ItemHolderInfo preLayoutInfo, @NonNull ItemHolderInfo postLayoutInfo) {
    return false;
  }

  @Override public void runPendingAnimations() {

  }

  @Override public void endAnimation(ViewHolder item) {

  }

  @Override public void endAnimations() {

  }

  @Override public boolean isRunning() {
    return false;
  }
}
